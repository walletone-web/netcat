# README #
This module is designed to receive payment orders for CMS Netcat

=== Wallet One Payment Gateway ===
Contributors: Wallet One
Version: 2.2.0
Tags: Wallet One, Netcat, buy now, payment, payment for Netcat, wallet one integration, Wallet One payment
Requires at least: 5.6
Tested up to: 5.7.0.17019
Stable tag: 5.7.0.17019
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.en.html

The Wallet One Payment Gateway module is a payment system for CMS Netcat. He it allows to pay for your orders on the site.

== Description ==
If you have an online store on CMS Netcat, then you need a module payments for orders made. This will help you plug the payment system Wallet One. With our system you will be able to significantly expand the method of receiving payment. This will lead to an increase in the number of customers to your store.

== Installation ==
1. Register on the site http://www.walletone.com
2. Download the module files on the site.
3. Instructions for configuring the plugin is in the file read.pdf.

== Screenshots ==
Screenshots are to be installed in the file read.pdf

== Changelog ==
= 1.0.1 =
* Fix - Внесены правки в модуль. Добавлен архив модуля

= 1.0.2 =
* Fix - Убрана оплата для минимагазина

= 1.0.3 =
* Fix - Fixed bug with request for payment system, change icons

= 2.0.0 =
* Fix - Fixed bug with new request from payment system, change classes

= 2.1.0 =
* Change expired date for invoice

= 2.2.0 =
* Add - added 54-fz support

== Frequently Asked Questions ==
No recent asked questions 

== Upgrade Notice ==
No recent asked updates

