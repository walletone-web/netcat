<?php
/**
 * Settings in admin
 * @author 
 */

class w1_admin {
	protected $MODULE_FOLDER, $MODULE_PATH;

	protected $settings;
  
  /**
   * Declaration of class client
   * 
   * @var object
   */
  protected $client;
  
  protected $logger;
  
  protected $form;

  public function __construct() {
		$nc_core = nc_Core::get_object();

		global $UI_CONFIG;

		$this->MODULE_FOLDER = $nc_core->MODULE_FOLDER;
		$this->MODULE_PATH = str_replace($nc_core->DOCUMENT_ROOT, "", $nc_core->MODULE_FOLDER)."w1/";

		$this->settings = $nc_core->get_settings('','w1');
    
    include_once __DIR__.'/walletone/Classes/W1Client.php';
    $this->client = \WalletOne\Classes\W1Client::init()->run(MAIN_LANG, 'netcat');
    
    $this->logger = \Logger::getLogger(__CLASS__);
    
    $this->getForm();
	}

  /**
   * Tab "Information".
   */
	public function info_show() {
    require_once ($this->MODULE_FOLDER."w1/page_info.php");
	}
  
  /**
   * Save tad "Insformation".
   * @return boolean
   */
	public function info_save() {
		return true;
	}

  /**
   * Tab "General settings".
   * @global type $UI_CONFIG
   */
	public function settings_show() {
    global $UI_CONFIG;
		$UI_CONFIG->add_settings_toolbar();
    $this->getForm();
    require_once ($this->MODULE_FOLDER."w1/page_settings.php");
	}
  
  protected function getForm(){
    $nc_core = nc_Core::get_object();
    
    $save = false;
    if(!empty($nc_core->input->fetch_get_post('act')) && $nc_core->input->fetch_get_post('act') == 'save'){
      $save = true;
    }
    $ptenabled = ($save ? $nc_core->input->fetch_get_post('WMI_PTENABLED')
            : unserialize($this->settings['WMI_PTENABLED']));
    $ptdisabled = ($save ? $nc_core->input->fetch_get_post('WMI_PTDISABLED')
            : unserialize($this->settings['WMI_PTDISABLED']));
			
	$taxes = [
      'tax_ru_1' => w1SettingsTaxTypeOptionNo,
      'tax_ru_2' => w1SettingsTaxTypeOption0,
      'tax_ru_3' => w1SettingsTaxTypeOption10,
      'tax_ru_4' => w1SettingsTaxTypeOption18,
      'tax_ru_5' => w1SettingsTaxTypeOption10Inc,
      'tax_ru_6' => w1SettingsTaxTypeOption18Inc,
    ];
	
    $this->form = [
      'generate' => [
        'label' => w1SettingsCreateIcon,
        'type' => 'checkbox',
        'value' => ($save ? $nc_core->input->fetch_get_post('generate')
            : $this->settings['generate']),
        'placeholder' => ''
      ],
      'WMI_MERCHANT_ID' => [
        'label' => w1SettingsMerchant,
        'type' => 'input',
        'value' => ($save ? $nc_core->input->fetch_get_post('WMI_MERCHANT_ID')
            : $this->settings['WMI_MERCHANT_ID']),
        'placeholder' => w1SettingsMerchantDesc
      ],
      'SIGNATURE_METHOD' => [
        'label' => w1SettingsSignatureMethod,
        'type' => 'select',
        'value' => ($save ? $nc_core->input->fetch_get_post('SIGNATURE_METHOD')
            : $this->settings['SIGNATURE_METHOD']),
        'options' => $this->client->getSettings()->signatureMethodArray,
        'placeholder' => w1SettingsSignatureMethodDesc
      ],
      'SIGNATURE' => [
        'label' => w1SettingsSignature,
        'type' => 'input',
        'value' => ($save ? $nc_core->input->fetch_get_post('SIGNATURE')
            : $this->settings['SIGNATURE']),
        'placeholder' => w1SettingsSignatureDesc
      ],
      'WMI_CURRENCY_ID' => [
        'label' => w1SettingsCurrency,
        'type' => 'select',
        'value' => ($save ? $nc_core->input->fetch_get_post('WMI_CURRENCY_ID')
            : $this->settings['WMI_CURRENCY_ID']),
        'options' => $this->client->getSettings()->currencyName,
        'placeholder' => ''
      ],
      'WMI_PTENABLED' => [
        'label' => w1SettingsPtenabled,
        'type' => 'groupCheckboxes',
        'html' => $this->client->getHtml()->getHtmlPayments('WMI_PTENABLED', $ptenabled, $this->client->getHtml()->filename)
      ],
      'WMI_PTDISABLED' => [
        'label' => w1SettingsPtdisabled,
        'type' => 'groupCheckboxes',
        'html' => $this->client->getHtml()->getHtmlPayments('WMI_PTDISABLED', $ptdisabled, $this->client->getHtml()->filename)
      ],
	  'enableKkt' => [
            'label' => w1SettingsEnableKkt,
            'type' => 'checkbox',
            'value' => ($save ? $nc_core->input->fetch_get_post('enableKkt')
                : $this->settings['enableKkt']),
            'placeholder' => ''
      ],
      'taxTypeDelivery' => [
          'label' => w1SettingsTaxTypeDelivery,
          'type' => 'select',
          'value' => ($save ? $nc_core->input->fetch_get_post('taxTypeDelivery')
              : $this->settings['taxTypeDelivery']),
          'options' => $taxes,
          'placeholder' => ''
      ]
    ];
  }

  /**
   * Save tab "General settings".
   */
	public function settings_save() {
		$nc_core = nc_core::get_object();
		foreach ($this->form as $key => $v) {
			$val = $nc_core->input->fetch_get_post($key);
			if (!is_array($val)){
        if(!empty($val) && preg_match('/[^a-zA-Z0-9]+$/ui', $val) == true) {
          continue;
        }
        else {
          if($key == 'generate' && !empty($val)) {
            if($pic = $this->client->createNewIcon($nc_core->input->fetch_get_post('WMI_PTENABLED'), $nc_core->input->fetch_get_post('WMI_PTDISABLED'))){
              global $db;
              $dbQuery = $db->get_row("SELECT * FROM `Classificator_PaymentSystem` WHERE `Value` LIKE 'nc_payment_system_w1'", ARRAY_A);
              if(!empty($dbQuery)){
                $dbQuery = $db->get_row("SELECT * FROM `Netshop_PaymentMethod` WHERE `PaymentSystem_ID` = ".$dbQuery['PaymentSystem_ID'], ARRAY_A);
                if(!empty($dbQuery)){
                  $method = new nc_netshop_payment_method($dbQuery['PaymentMethod_ID']);
                  $desc = $method->get('description');
                  if (strpos($desc, '<img') === false) {
                    $desc .= ' <img src="' . $this->MODULE_PATH . 'walletone/img/' . $pic . '">';
                  }
                  else{
                    $desc = preg_replace('/="(.*?)"/ui', '="'.$this->MODULE_PATH . 'walletone/img/' . $pic.'"', $desc);
                  }
                  $method->set('description', $desc);
                  $method->save();
                }
              }
            }
          }
          $nc_core->set_settings($key, $val, 'w1');
        }
			} 
      else {
				$nc_core->set_settings( $key, serialize($val), 'w1' );
			}
		}

		$this->settings = $nc_core->get_settings('','w1');
	}
}