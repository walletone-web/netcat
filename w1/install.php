<?php

/**
 * Installation file module.
 */
include_once($_SERVER['DOCUMENT_ROOT'] . "/vars.inc.php");
if (!isset($NETCAT_FOLDER)) {
  $NETCAT_FOLDER = realpath(dirname(__FILE__) . '/..') . DIRECTORY_SEPARATOR;
}

define("MAIN_LANG", "ru");

// unset for security reasons
$SYSTEM_FOLDER = "";
// if vars.inc.php not updated set default value for $SYSTEM_FOLDER
if (!$SYSTEM_FOLDER) {
  global $SYSTEM_FOLDER;
  $SYSTEM_FOLDER = $ROOT_FOLDER . "system/";
}

// include all new system classes and get nc_core object
require_once($INCLUDE_FOLDER . "unicode.inc.php");
require_once($SYSTEM_FOLDER . "index.php");

// set db for compatibility
global $db;
$db = $nc_core->db;
InstallThisModule();

/**
 * 
 * @global type $TMP_FOLDER
 * @global type $MODULE_FOLDER
 * @global type $ROOT_FOLDER
 * @global type $DOCUMENT_ROOT
 * @global type $Keyword
 */
function copy_files() {
  file_put_contents($_SERVER['DOCUMENT_ROOT'].'/1.txt', '000000000000'."\n");
  global $TMP_FOLDER, $MODULE_FOLDER, $ROOT_FOLDER, $DOCUMENT_ROOT;
  global $Keyword;

  $TMP_FOLDER1 = __DIR__;
  if (preg_match('/^[\\\]+$/', $TMP_FOLDER1) !== FALSE) {
    $TMP_FOLDER1 = preg_replace('/^[\\\]+$/', '/', $TMP_FOLDER1);
  }
  $Keyword = 'w1';
  if (!is_dir($MODULE_FOLDER . $Keyword)) {
    @mkdir($MODULE_FOLDER . $Keyword, 0775);
  }

  $FileList = "/files.txt";
  $fp = fopen($TMP_FOLDER1 . $FileList, "r");
  // могут быть поддиректории (скрипт корректно обрабатывает только один уровень)
  while (!feof($fp)) {
    $file_name = chop(fgets($fp, 4096));
    file_put_contents($_SERVER['DOCUMENT_ROOT'].'/1.txt', $file_name." ----\n", FILE_APPEND);
    if (strlen($file_name) == 0) {
      break;
    }
    // get directory and file name
    preg_match("!.+/$Keyword/(\w+/)?(.+)$!", $file_name, $regs);
    if ($regs[1] && strpos($regs[1], 'walletone') == false) { // subdirectory
      file_put_contents($_SERVER['DOCUMENT_ROOT'].'/1.txt', '2222222'."\n", FILE_APPEND);
      $dir = $MODULE_FOLDER . $Keyword . "/" . $regs[1];
      if (!is_dir($dir)) {
        mkdir($dir, 0775);
      }
    }
    if($regs[2]){
      $src = $TMP_FOLDER1 . '/'. $regs[2];
    }
    else{
      $src = $TMP_FOLDER1 . $file_name;
    }
    
    file_put_contents($_SERVER['DOCUMENT_ROOT'].'/1.txt', $src." ******\n", FILE_APPEND);
    if (DIRECTORY_SEPARATOR == '\\') {
      $src = str_replace('/', '\\', $src);
    }
    $dst = $DOCUMENT_ROOT . $file_name;
    if (DIRECTORY_SEPARATOR == '\\') {
      $dst = str_replace('/', '\\', $dst);
    }
    file_put_contents($_SERVER['DOCUMENT_ROOT'].'/1.txt', $src.' || '.$dst." ++++\n", FILE_APPEND);
    if (file_exists($src)) {
      file_put_contents($_SERVER['DOCUMENT_ROOT'].'/1.txt', '4444444'."\n", FILE_APPEND);
      copy($src, $dst);
    }
  }
  fclose($fp);
}

/**
 * The copying files from a subdirectories
 * 
 * @param type $src
 * @param type $dst
 */
function rcopy($src, $dst) {
  if (file_exists($dst)) {
    rmdir($dst);
  }
  if (is_dir($src)) {
    mkdir($dst);
    $files = scandir($src);
    foreach ($files as $file){
      if ($file != "." && $file != "..") {
        rcopy("$src/$file", "$dst/$file");
      }
    }
  }
  elseif(file_exists($src)) {
    $res = copy($src, $dst);
  }
}

/**
 * 
 * @global type $ADMIN_FOLDER
 * @global type $TMP_FOLDER
 * @global type $Parameters
 * @global type $db
 * @return type
 */
function InstallThisModule() {
  global $ADMIN_FOLDER, $DOCUMENT_ROOT, $db, $MODULE_FOLDER;
  require_once $ADMIN_FOLDER . "class/import.inc.php";

  require_once(MAIN_LANG . ".lang.php");

  if (!@require_once(MAIN_LANG . ".lang.php")) {
    require_once("en.lang.php");
  }

  $db->query("INSERT INTO `Module` (`Module_Name`, `Keyword`, `Description`,
      `Parameters`, `Example_URL`, `Help_URL`, `Installed`, `Number`, `Inside_Admin`, `Checked`)
      VALUES ('W1', 'w1', 'W1_DESCRIPTION', 'ADMIN_SETTINGS_LOCATION=module.w1.settings', '', '/settings/modules/w1/', 1, '', 0, 1)");

  $db->query("INSERT INTO `Classificator_PaymentSystem` (`PaymentSystem_Name`, `PaymentSystem_Priority`, `Value`, `Checked`) 
	VALUES ('Wallet One', '1', 'nc_payment_system_w1', '1')");

  $db->query("INSERT INTO `Settings`
			(`Key`, `Value`, `Module`)
		VALUES
      ('generate', '', 'w1'),
      ('WMI_MERCHANT_ID', '', 'w1'),
      ('SIGNATURE_METHOD', '', 'w1'),
      ('SIGNATURE', '', 'w1'),
      ('WMI_CURRENCY_ID', '', 'w1'),
      ('WMI_PTENABLED', '', 'w1'),
      ('WMI_PTDISABLED', '', 'w1'),
	  ('enableKkt', '', 'w1'),
      ('taxTypeDelivery', '', 'w1');
  ");

  copy_files();
  
  if (!is_dir($MODULE_FOLDER.'w1/log')){
    @mkdir($MODULE_FOLDER.'w1/log', 0775);
  }
  if (!is_dir($MODULE_FOLDER.'w1/tmp')){
    @mkdir($MODULE_FOLDER.'w1/tmp', 0775);
  }
  
  rcopy(__DIR__ . '/walletone', $MODULE_FOLDER . 'w1/walletone');

  $result ["Success"] = 1;
  $result ["ErrorMessage"] = NETCAT_MODULE_ERROR;

  return $result;
}
