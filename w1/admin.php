<?php
/**
 * The administrative interface module.
 * @author Wallet One
 */

$NETCAT_FOLDER = join( strstr(__FILE__, "/") ? "/" : "\\", array_slice( preg_split("/[\/\\\]+/", __FILE__), 0, -4 ) ).( strstr(__FILE__, "/") ? "/" : "\\" );
include_once ($NETCAT_FOLDER."vars.inc.php");
require ($ADMIN_FOLDER."function.inc.php");

// UI config
require_once ($ADMIN_FOLDER."modules/ui.php");
require_once ($MODULE_FOLDER."w1/ui_config.php");
require_once ($MODULE_FOLDER."w1/admin.inc.php");

if(!defined(W1)) {
  if (is_file($MODULE_FOLDER."w1/".MAIN_LANG.".lang.php")) {
    require_once ($MODULE_FOLDER."w1/".MAIN_LANG.".lang.php");
  } 
  else {
    require_once ($MODULE_FOLDER."w1/"."en.lang.php");
  }
}

include_once __DIR__.'/walletone/Classes/W1Client.php';
$client = \WalletOne\Classes\W1Client::init()->run(MAIN_LANG, 'netcat');

$w1_admin = new w1_admin();

if ( !$view ) $view = 'info';
$Title1 = NETCAT_MODULES;
$Title2 = W1;

$AJAX_SAVER = !( $perm->isGuest() || $view == 'info');

BeginHtml ($Title2, $Title1, "http://".$DOC_DOMAIN."/settings/modules/w1/", 'w1');

$perm->ExitIfNotAccess(NC_PERM_MODULE, 0, 0, 0, 1);
$UI_CONFIG = new ui_config_module_w1($view, '');

//TODO: Name methods
$method_show = $view."_show";
$method_save = $view."_save";

if ( !is_callable( array($w1_admin, $method_show)) || !is_callable( array($w1_admin, $method_save)) ) {
	nc_print_status("Incorrect view: ".  htmlspecialchars($view), 'error');
	exit;
}

//TODO: Save information
if ( $nc_core->input->fetch_get_post('act') === 'save' ) {
	try {
		$w1_admin->$method_save();
		nc_print_status(w1SettingsAdminSaveOk, 'ok');
	}
	catch ( Exception $e ) {
		nc_print_status($e->getMessage(), 'error');
	}
}

//TODO: Show form method
$w1_admin->$method_show();

EndHtml();