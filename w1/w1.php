<?php

class nc_payment_system_w1 extends nc_payment_system {
  // параметры сайта в платежной системе
  protected $settings = array();
  
  protected $automatic = TRUE;

  // принимаемые валюты
  protected $accepted_currencies = array('RUB', 'RUR', 'USD', 'EUR');
  protected $currency_map = array('RUR' => 'RUB');
  
  /**
   * Declaration of class client
   * 
   * @var object
   */
  protected $client;
  
  protected $logger;
  
  public function __construct() {
    $nc_core = nc_Core::get_object();
    $this->settings = $nc_core->get_settings('','w1');
    
    include_once dirname(__DIR__).'/../../w1/walletone/Classes/W1Client.php';
    $this->client = \WalletOne\Classes\W1Client::init()->run(MAIN_LANG, 'netcat');
    
    $this->logger = \Logger::getLogger(__CLASS__);
  }

  /**
   * Creating forms for payment.
   */
  public function execute_payment_request(nc_payment_invoice $invoice) {
    $nc_core = nc_core::get_object();
    
    $url = $this->get_callback_script_url().(strpos($this->get_callback_script_url(), 'paySystem=nc_payment_system_w1') === false ? '?paySystem=nc_payment_system_w1' : '');
    $url .= (strpos($url, '?') == false ? '?' : '&').'id_order='.$invoice->get_id();
    
    $settings = array(
      'merchantId' => $nc_core->get_settings('WMI_MERCHANT_ID', 'w1'),
      'signatureMethod' => $nc_core->get_settings('SIGNATURE_METHOD', 'w1'),
      'signature' => $nc_core->get_settings('SIGNATURE', 'w1'),
      'currencyId' => $nc_core->get_settings('WMI_CURRENCY_ID', 'w1'),
      'currencyDefault' => '',
      'orderStatusSuccess' => '',
      'orderStatusWaiting' => '',
      //'cultureId' => 'ru-RU',
      'order_currency' => ($invoice->currency == 'RUR' ? 'RUB' : $invoice->currency),
      'summ' => number_format($invoice->get_amount("%0.2F"), 2, '.', ''),
      'orderId' =>  $invoice->get_id(),
      'siteName' => '',
      'nameCms' => '_netcat',
      'successUrl' => $url,
      'failUrl' => $url,
      'module' => 'shop'
    );
    
    if(!empty($nc_core->get_settings('WMI_PTENABLED', 'w1'))){
      $settings['paymentSystemEnabled'] = unserialize($nc_core->get_settings('WMI_PTENABLED', 'w1'));
    }
    if(!empty($nc_core->get_settings('WMI_PTDISABLED', 'w1'))){
      $settings['paymentSystemDisabled'] = unserialize($nc_core->get_settings('WMI_PTDISABLED', 'w1'));
    }
    if(!empty($invoice->customer_email)) {
      $settings['emailBuyer'] = $invoice->customer_email;
    }
    
    if ($this->client->validateParams($settings) !== true) {
      echo $this->client->getMessage($this->client->errors, $this->client->messages, 'html');
      $this->add_error($this->client->getMessage($this->client->errors, $this->client->messages, 'text'));
      return;
    }

    $fieldsForm = $this->client->createFieldsForForm();
    $form = $this->client->createHtmlForm($fieldsForm, true);
    
    ob_end_clean();
    echo $form;
    exit();
  } 

  /**
   * Checking for required fields.
   */
  public function validate_payment_request_parameters() {
    if (empty($this->settings['WMI_MERCHANT_ID'])) {
      $this->logger->info(sprintf(w1ErrorRequired, w1SettingsMerchant));
      $this->add_error(sprintf(w1ErrorRequired, w1SettingsMerchant));
    }
    elseif (empty($this->settings['SIGNATURE'])) {
      $this->logger->info(sprintf(w1ErrorRequired, w1SettingsSignature));
      $this->add_error(sprintf(w1ErrorRequired, w1SettingsSignature));
    }
  }

  /**
   * Check for a response from the payment system.
   *
   * @param nc_payment_invoice $invoice
   * @return
   */
  public function validate_payment_callback_response(nc_payment_invoice $invoice = null) {
    $this->logger->info('111111111111');
    if(empty($_POST) && empty($_GET['id_order'])){
      $this->add_error(w1ErrorEmptyPost);
      if(empty($_POST)){
        ob_start();
        header('Status: 200 OK');
        header('HTTP/1.0 200 OK');
        echo 'WMI_RESULT=RETRY&WMI_DESCRIPTION='.w1ErrorEmptyPost;
        die();
      }
      else{
        echo 'WMI_RESULT=RETRY&WMI_DESCRIPTION='.w1ErrorEmptyPost;
      }
    }
    elseif(empty($_POST) && !empty($_GET['id_order']) && is_numeric($_GET['id_order'])){
      $invoice = new nc_payment_invoice();
      $invoice->load((int)$_GET['id_order']);
      $text = sprintf(w1OrderResultCreatedOnlyText, $_GET['id_order']);
      if($invoice->get('status') == $invoice::STATUS_SUCCESS){
        $text = sprintf(w1OrderResultSuccessOnlyText, $_GET['id_order']);
      }
      $this->logger->info($text);
      $this->on_response($invoice);
    }
    elseif(!empty($_POST)){
      $nc_core = nc_core::get_object();
      $settings = [
        'merchantId' => $nc_core->get_settings('WMI_MERCHANT_ID', 'w1'),
        'signatureMethod' => $nc_core->get_settings('SIGNATURE_METHOD', 'w1'),
        'signature' => $nc_core->get_settings('SIGNATURE', 'w1'),
        'currencyId' => $nc_core->get_settings('WMI_CURRENCY_ID', 'w1'),
        'currencyDefault' => '',
        'orderStatusSuccess' => '',
        'orderStatusWaiting' => ''
      ];
      
      $settings['orderPaymentId'] = $_POST['WMI_ORDER_ID'];
      $settings['orderState'] = mb_strtolower($_POST['WMI_ORDER_STATE']);
      $settings['orderId'] = str_replace('_' . $_SERVER['HTTP_HOST'], '', $_POST['WMI_PAYMENT_NO']);
      $settings['paymentType'] = $_POST['WMI_PAYMENT_TYPE'];
      $settings['summ'] = $_POST['WMI_PAYMENT_AMOUNT'];
      if ($this->client->resultValidation($settings, $_POST) == true) {
        $result = $this->client->getResult();
        //checking on the order amount
        if ($result->summ != number_format($invoice->get('amount'), 2, '.', '')) {
          $error = sprintf(w1ErrorResultOrderSumm, $result->orderId, $result->orderState, $result->orderPaymentId);
          $this->logger->info($error);
          $invoice->set('status', nc_payment_invoice::STATUS_CALLBACK_WRONG_SUM)->save();
          ob_start();
          echo 'WMI_RESULT=RETRY&WMI_DESCRIPTION=' . $error;
          die();
        }
        if ($invoice->get('status') != $invoice::STATUS_SUCCESS && $result->orderState = 'acepted') {
          $text = sprintf(w1OrderResultSuccess, $result->orderId, $result->orderState, $result->orderPaymentId) . ' ' . $_POST['WMI_PAYMENT_TYPE'];
          $this->logger->info($text);
          $invoice->set('status', nc_payment_invoice::STATUS_SUCCESS)->save();
          $this->on_payment_success($invoice);
          ob_start();
          echo 'WMI_RESULT=OK';
          die();
        }
        else{
          $invoice->set('status', nc_payment_invoice::STATUS_CALLBACK_ERROR)->save();
        }
      }
      $this->logger->info('Error');
      ob_start();
      echo 'WMI_RESULT=RETRY&WMI_DESCRIPTION=Error';
      die();
    }
  }
  
  /**
   * Check the status of a response from the payment system.
   *
   * @param nc_payment_invoice $invoice
   * @return
   */
  public function on_response(nc_payment_invoice $invoice = null) {
    if(!empty($invoice->get('status')) && $invoice->get('status') == $invoice::STATUS_SUCCESS){
      echo '<html>
              <head>
                <title>Оплата заказа</title>
                <style>
                  p {
                    text-align: center;
                    top: 15%;
                    position: relative;
                    font-weight: bold;
                    font-size: 20px;
                  }
                  a {
                    color: red;
                  }
                </style>
              </head>
              <body>
                <p>Ваш заказ оплачен. Перейти на <a href="/">сайт</a>.</p>
              </body>
            </html>';
    }
    elseif (!empty($invoice->get('status')) && $invoice->get('status') != $invoice::STATUS_SUCCESS) {
      echo '<html>
              <head>
                <title>Оплата заказа</title>
                <style>
                  p {
                    text-align: center;
                    top: 15%;
                    position: relative;
                    font-weight: bold;
                    font-size: 20px;
                  }
                  a {
                    color: red;
                  }
                </style>
              </head>
              <body>
                <p>Ваш заказ ожидает оплаты. Перейти на <a href="/">сайт</a>.</p>
              </body>
            </html>';
    } 
    else {
      $this->on_payment_failure($invoice);
    }
  }

  public function load_invoice_on_callback() {
    return $this->load_invoice(str_replace('_netcat', '', $this->get_response_value('WMI_PAYMENT_NO')));
  }

}
